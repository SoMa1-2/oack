//
//  DevilMageSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 19..
//
//

#ifndef __TapHero__DevilMageSprite__
#define __TapHero__DevilMageSprite__

#define DEVIL_NORMAL "devil_normal.png"

#define DEVIL_IDLE "devil_idle.plist"
#define DEVIL_IDLE_FRAME_SIZE 2

#define DEVIL_ATK "devil_atk.plist"
#define DEVIL_ATK_FRAME_SIZE 1

#define DEVIL_BACK "devil_back.plist"
#define DEVIL_BACK_FRAME_SIZE 4

#include <stdio.h>
#include "WarriorSprite.h"
#include "SpriteFrameManager.h"

class DevilMage : public Warrior {
    
public:
    static DevilMage* create(){
        auto t = new DevilMage() ;
        
        if (t->initWithFile(DEVIL_NORMAL)) {
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
    void attack() ;
    
private:
    DevilMage() ;
    ~DevilMage() ;
};

#endif /* defined(__TapHero__DevilMageSprite__) */
