//
//  StageSelectScene.h
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 9. 29..
//
//

#ifndef __STAGESELECT_SCENE_H__
#define __STAGESELECT_SCENE_H__

#include "cocos2d.h"
#include "CocosGUI.h"

USING_NS_CC;
using namespace ui;

class StageSelectScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    Size visibleSize;
    Size winSize;
    Vec2 origin;
    
    CREATE_FUNC(StageSelectScene);
};

#endif /* defined(__STAGESELECT_SCENE_H__) */
