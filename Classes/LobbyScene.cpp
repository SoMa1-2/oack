//
//  LobbyScene.cpp
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 10. 5..
//
//

#include "LobbyScene.h"
#include "StageSelectScene.h"
#include "DevilMageSprite.h"

Scene* LobbyScene::createScene()
{
    auto scene = Scene::create();
    auto layer = LobbyScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool LobbyScene::init()
{
    //initWithColor를 사용하여 scene 배경색 설정
    if (!LayerColor::initWithColor(Color4B::GRAY))
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    winSize = Director::getInstance()->getWinSize();
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(LobbyScene::onTouchBegan, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    //상단바
    auto blackbg = Sprite::create("blacktile.png");
    blackbg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    blackbg->setPosition(Vec2(origin.x, visibleSize.height+origin.y));
    blackbg->setScale(visibleSize.width/blackbg->getContentSize().width,20/blackbg->getContentSize().height);
    this->addChild(blackbg);
    
    auto topLabel = Label::createWithTTF("Name : Mawang", "fonts/RetroComputer.ttf", 12);
    topLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    topLabel->setPosition(Vec2(origin.x, visibleSize.height));
    topLabel->setAlignment(TextHAlignment::CENTER);
    topLabel->setColor(Color3B::WHITE);
    this->addChild(topLabel);
    
    //캐릭터 대기실 부분
    auto warrior = DevilMage::create();
    warrior->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    warrior->setPosition(Vec2(origin.x, visibleSize.height*3/5+origin.y));
    
    warrior->runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.7),JumpBy::create(3.0, Point(visibleSize.width-warrior->getContentSize().width,0),50,3),FlipX::create(true),DelayTime::create(0.7),JumpBy::create(3.0, Point(-visibleSize.width+warrior->getContentSize().width,0),50,3),FlipX::create(false),NULL)));
    this->addChild(warrior);
    
    //게임시작 버튼
    startBtn = ui::Scale9Sprite::create("buttonL.png");
    startBtn->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
    startBtn->setPosition(Vec2(visibleSize.width/2+origin.x, visibleSize.height*3/5+origin.y));
    startBtn->setContentSize(Size(visibleSize.width,50.0));
    this->addChild(startBtn);
    
    auto startLabel = Label::createWithTTF("GO DUNGEON", "fonts/RetroComputer.ttf", 14);
    startLabel->setPosition(Vec2(visibleSize.width/2.0+origin.x, visibleSize.height*3/5+origin.y-25.0));
    startLabel->setAlignment(TextHAlignment::CENTER);
    startLabel->setColor(Color3B::WHITE);
    this->addChild(startLabel);

    //하단 버튼
    for(int i=0;i<3;i++)
    {
        botBtn[i] = Sprite::create(i==0?"buttonL2rev.png":"buttonL2.png");
        botBtn[i]->setScale(visibleSize.width/3/botBtn[i]->getContentSize().width, 25/botBtn[i]->getContentSize().height);
        botBtn[i]->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        botBtn[i]->setPosition(Vec2(origin.x+visibleSize.width*i/3,origin.y));
        this->addChild(botBtn[i]);
        
        auto startLabel = Label::createWithTTF(i==0?"Menu R":i==1?"Menu G":"Menu B", "fonts/RetroComputer.ttf", 9);
        startLabel->setPosition(Vec2(origin.x+visibleSize.width*(i/3.0+1.0/6.0), 25.0/2.0));
        startLabel->setAlignment(TextHAlignment::CENTER);
        startLabel->setColor(Color3B::WHITE);
        this->addChild(startLabel);
    }
    
    //하단 스크롤뷰
    for(int i=0;i<3;i++)
    {
        scroll[i] = ui::ScrollView::create();
        scroll[i]->setBackGroundColorType(LAYOUT_COLOR_SOLID);
        scroll[i]->setBackGroundColor(Color3B(200, 200, 200));
        scroll[i]->setSize(Size(visibleSize.width, visibleSize.height*2/5-10));
        scroll[i]->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        scroll[i]->setPosition(Vec2(origin.x, 25+origin.y));
        scroll[i]->setDirection(SCROLLVIEW_DIR_VERTICAL);
        scroll[i]->setSwallowTouches(false);
    }
    
    //스크롤뷰 내용
    Layer *container[3];
    for(int i=0;i<3;i++)
    {
        container[i] = Layer::create();
        for(int j=0;j<10;j++)
        {
            char str[10];
            sprintf(str,"menu %d",j+1);
            auto label = Label::createWithTTF(str, "fonts/RetroComputer.ttf", 14);
            label->setColor(i==0?Color3B::RED:i==1?Color3B::GREEN:Color3B::BLUE);
            label->setPosition(Vec2(visibleSize.width/2, (9-j)*40+20));
            container[i]->addChild(label);
        }
        scroll[i]->addChild(container[i]);
        scroll[i]->setInnerContainerSize(Size(visibleSize.width, 400));
        
        this->addChild(scroll[i]);
    }
    
    scroll[0]->setVisible(true);
    scroll[1]->setVisible(false);
    scroll[2]->setVisible(false);
    
    isSelected[0]=true, isSelected[1]=false, isSelected[2]=false;
    
    return true;
}

bool LobbyScene::onTouchBegan(Touch *touch, Event *unused_event)
{
    if(startBtn->boundingBox().containsPoint(touch->getLocation()))
    {
        Director::getInstance()->pushScene(StageSelectScene::createScene());
    }
    for(int i=0;i<3;i++)
    {
        if(botBtn[i]->boundingBox().containsPoint(touch->getLocation()) && isSelected[i]==false)
        {
            isSelected[0]=isSelected[1]=isSelected[2]=false;
            botBtn[0]->setTexture(Director::getInstance()->getTextureCache()->addImage("buttonL2.png"));
            botBtn[1]->setTexture(Director::getInstance()->getTextureCache()->addImage("buttonL2.png"));
            botBtn[2]->setTexture(Director::getInstance()->getTextureCache()->addImage("buttonL2.png"));
            scroll[0]->setVisible(false);
            scroll[1]->setVisible(false);
            scroll[2]->setVisible(false);
            
            isSelected[i]=true;
            botBtn[i]->setTexture(Director::getInstance()->getTextureCache()->addImage("buttonL2rev.png"));
            scroll[i]->setVisible(true);
            
            break;
        }
    }
    return true;
}