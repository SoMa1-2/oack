//
//  CoinSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 16..
//
//

#ifndef __TapHero__CoinSprite__
#define __TapHero__CoinSprite__

#define COIN_NORMAL "coin_normal.png"

#define COIN_SHINE "coin_shine.plist"
#define COIN_SHINE_FRAME_SIZE 8

#define COIN_SPIN "coin_spin.plist"
#define COIN_SPIN_FRAME_SIZE 4

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC ;

class Coin : public Sprite{
    
public:
    static Coin* create(int gold){
        auto t = new Coin() ;
        
        if (t->initWithFile(COIN_NORMAL)) {
            t->gold = gold ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
    void drop() ;
    void shine() ;
    void spin() ;
    void move() ;
    void done() ;
    
    int gold ;
    Animation *shineAnim, *spinAnim ;
    
private:
    Coin() ;
    ~Coin() ;
};

#endif /* defined(__TapHero__CoinSprite__) */
