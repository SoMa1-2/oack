//
//  TitleScene.cpp
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 9. 17..
//
//

#include "TitleScene.h"
#include "LobbyScene.h"

Scene* TitleScene::createScene()
{
    auto scene = Scene::create();
    auto layer = TitleScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool TitleScene::init()
{
    //initWithColor를 사용하여 scene 배경색 설정
    if (!LayerColor::initWithColor(Color4B::BLACK))
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    //bgm
    //SimpleAudioEngine::getInstance()->preloadBackgroundMusic("aaaaa.bgm");
    SimpleAudioEngine::getInstance()->playBackgroundMusic("Focus.mp3", true);
    
    // 타이틀 이미지 sprite
    auto titleImg = Sprite::create("title.png");
    
    titleImg->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    titleImg->setPosition(Vec2(visibleSize.width/2+origin.x, visibleSize.height/2+70));
    this->addChild(titleImg);
    
    // 터치 안내 label
    touchLabel = Label::createWithTTF("Please Touch", "fonts/RetroComputer.ttf", 14);
    
    touchLabel->setPosition(Vec2(visibleSize.width/2+origin.x, visibleSize.height/2-50));
    touchLabel->setAlignment(TextHAlignment::CENTER);
    touchLabel->setColor(Color3B::WHITE);
    
    //label 액션
    touchLabel->runAction(RepeatForever::create(Sequence::create(DelayTime::create(0.7),FadeOut::create(0.0f),DelayTime::create(0.7),FadeIn::create(0.0f),NULL)));
    
    this->addChild(touchLabel);
    
    // 화면 터치시 onTouchBegan()호출
    listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(TitleScene::onTouchBegan, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    loginFB = Sprite::create("LoginFacebook.png");
    loginFB->setPosition(Vec2(visibleSize.width/2+origin.x, visibleSize.height/2-70));
    loginFB->setVisible(false);
    this->addChild(loginFB);
    
    return true;
}

bool TitleScene::onTouchBegan(Touch *touch, Event *unused_event)
{
    touchLabel->setVisible(false);
    loginFB->setVisible(true);
    if(loginFB->boundingBox().containsPoint(touch->getLocation()))
    {
        Director::getInstance()->replaceScene(LobbyScene::createScene());
    }
    return true;
}