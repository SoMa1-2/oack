//
//  SkillIconSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 21..
//
//

#ifndef __TapHero__SkillIconSprite__
#define __TapHero__SkillIconSprite__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC ;

typedef struct{
    int skillType ;
    int costGuage ;
    float skillCooltime ;
    std::string skillIconPath ;
}SKILL_INFO ;

enum SKILL_TYPE{
    SUMMON_MONSTER,
    METEOR
};

class Skill : public Sprite{
    
public:
    static Skill* create(SKILL_INFO skillData){
        auto t = new Skill() ;
        t->skillData = skillData ;
        
        if (t->initWithFile(t->skillData.skillIconPath)) {
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
    void useSkill() ;
    
private:
    Skill() ;
    ~Skill() ;
    
    SKILL_INFO skillData ;
    
    ProgressTimer *coolTimeProgress ;
    ProgressTimer *comboGuage ;
    
    void initCoolTimeProgress() ;
    
    void checkSkillCost(Ref *sender) ;
    void cooldownDone() ;
    
    void showShineEffect() ;
    
    bool isOverCost, isCooldown ;
};

#endif /* defined(__TapHero__SkillIconSprite__) */
