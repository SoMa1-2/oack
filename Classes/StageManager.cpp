//
//  StageManager.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 21..
//
//

#include "StageManager.h"

static StManager *manager = nullptr ;
StManager* StManager::getInstance(){
    if(!manager)
        manager = new StManager() ;
    
    manager->currWavePosition = 0 ;
    return manager ;
}

void StManager::init(){
    currWavePosition = 0 ;
    readFromJson() ;
}

void StManager::readFromJson(){
    
    FileUtils* fileUtils = FileUtils::getInstance();
    ssize_t fileSize;
    unsigned char *stream = fileUtils->getFileData("stagejson/stage_1.json", "rb", &fileSize);
    
    Document doc;
    rapidjson::Value::MemberIterator M;
    
    doc.Parse<0>((const char*)stream);
    
    stageTitle = doc[TITLE].GetString() ;
    waveCount = doc[WAVES].Size() ;
    
    for(int i=0; i<waveCount; i++){
        std::vector<std::vector<std::map<const char*, double>>> row ;
        
        for(int j=0; j<doc[WAVES][i].Size(); j++){
            std::vector<std::map<const char*, double>> obj ;
            
            for (M=doc[WAVES][i][j].MemberBegin(); M!=doc[WAVES][i][j].MemberEnd(); M++){
                
                std::map<const char*, double> tmpMap ;
                
                const char *key   = M->name.GetString();
                const double value = M->value.GetDouble();
                
                if (key!=NULL){
                    tmpMap[key] = value ;
                    obj.push_back(tmpMap) ;
                }
            }
            row.push_back(obj) ;
        }
        table.push_back(row) ;
    }
}

Vector<Monster*> StManager::getCurrentWaveMonsters(){
    
    std::vector<std::vector<std::map<const char*, double>>> row = table.at(currWavePosition) ;
    
    bool tickTock = true ;
    
    for(int i=0; i<row.size(); i++){
        std::vector<std::map<const char*, double>> obj = row.at(i) ;
        for(std::map<const char*, double> tmpMap : obj){
            
            for(auto imap: tmpMap){
                const char *key = imap.first ;
                double value = imap.second ;
                
                if(tickTock){
                    for(int i=0; i<(int)value; i++){
                        Monster *monster ;
                        
                        CCLOG("%s // %f", key, value);
                        if(strcmp(key, KEY_A_KNIGHT) == 0)
                            monster = AxeKnight::create() ;
                        else
                            monster = SwordKnight::create() ;
                        
                        tempMonsters.pushBack(monster) ;
                    }

                }else{
                    
                    int delayCnt = 0 ;
                    for(Monster *monster : tempMonsters){
                        monster->setDelay(value*delayCnt++) ;
                    }
                    
                    monsters.pushBack(tempMonsters) ;
                    tempMonsters.clear() ;
                }
                
                tickTock = !tickTock ;
            }
        }
    }
    
    return monsters ;
}