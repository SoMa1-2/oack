//
//  MonsterSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 13..
//
//

#ifndef __TapHero__MonsterSprite__
#define __TapHero__MonsterSprite__

#define GOLD_UNIT 30

#include <stdio.h>
#include "WarriorSprite.h"
#include "cocos2d.h"

USING_NS_CC;

class Monster : public Sprite{
public:
    
    virtual bool initWithFile(const std::string &filename);
    
    void initHealthBar();
    
    void attacked(Warrior::ATTACK_INFO *damage);
    void healing(int heal);
    float getCrrHp();
    
    Action *onwardAnim, *hitAnim, *dieAnim;
    
    void onward();
    void die();
    void addDust() ;
    void move(float dt) ;
    
    void setDelay(float delay) ;
    void onSchedule() ;
    
    CREATE_FUNC(Monster);
    
    int monsterMaxHp, monsterCurrHp;
    int monsterMoveSpeed;
    int monsterGold;
    
    bool isDie, isAddDust ;
    
    ProgressTimer *monsterHpBar;
    
private:
    SpriteFrameCache *spritecache;
    
};

#endif /* defined(__TapHero__MonsterSprite__) */
