//
//  AxeKnightSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#ifndef __TapHero__SwordKnightSprite__
#define __TapHero__SwordKnightSprite__

#define S_KNIGHT_NORMAL "sword_knight_normal.png"

#define S_KNIGHT_ONWARD "sword_knight_walk.plist"
#define S_KNIGHT_ONWARD_FRAME_SIZE 4

#define S_KNIGHT_HIT "sword_knight_hit.plist"
#define S_KNIGHT_HIT_FRAME_SIZE 1

#define S_KNIGHT_DIE "axe_knight_die.plist"
#define S_KNIGHT_DIE_FRAME_SIZE 5


#include <stdio.h>
#include "cocos2d.h"
#include "MonsterSprite.h"

USING_NS_CC ;

class SwordKnight : public Monster{
    
public:
    static SwordKnight* create(){
        auto t = new SwordKnight() ;
        
        if (t->initWithFile(S_KNIGHT_NORMAL)) {
            t->initHealthBar() ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
private:
    SwordKnight() ;
    ~SwordKnight() ;
};

#endif /* defined(__TapHero__AxeKnightSprite__) */
