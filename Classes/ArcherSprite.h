//
//  ArcherSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 15..
//
//

#ifndef __TapHero__ArcherSprite__
#define __TapHero__ArcherSprite__

#define ARCHER_NORMAL "archer_normal.png"

#define ARCHER_IDLE "archer_idle.plist"
#define ARCHER_IDLE_FRAME_SIZE 4

#define ARCHER_ATK "archer_atk.plist"
#define ARCHER_ATK_FRAME_SIZE 5

#define ARCHER_RETREAT "archer_roll.plist"
#define ARCHER_RETREAT_FRAME_SIZE 5

#include <stdio.h>
#include "WarriorSprite.h"
#include "SpriteFrameManager.h"

class Archer : public Warrior {
    
public:
    static Archer* create(){
        auto t = new Archer() ;
        
        if (t->initWithFile(ARCHER_NORMAL)) {
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
private:
    Archer() ;
    ~Archer() ;

};

#endif /* defined(__TapHero__ArcherSprite__) */
