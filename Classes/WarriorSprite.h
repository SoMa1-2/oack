//
//  WarriorSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 13..
//
//

#ifndef __TapHero__WarriorSprite__
#define __TapHero__WarriorSprite__

#include <stdio.h>
#include "cocos2d.h"
#include "SkillSprite.h"

USING_NS_CC ;

class Warrior : public Sprite{
public:
    
    typedef struct{
        int atkDamage ;
        bool isCritical ;
    }ATTACK_INFO ;
    
    enum CLASS_TYPE{
        KNIGHT,
        ARCHER,
        MAGE
    };
    
    Vector<Skill*> skills ;
    
    virtual bool initWithFile(const std::string &filename);
    CREATE_FUNC(Warrior) ;
    
    Sprite* getAttackObj() ;
    void attakObjDone(Ref *sender) ;
    
    void attack() ;
    void idle() ;
    void retreat() ;
    void die() ;
    
    Animation *idleAnim, *attackAnim, *retreatAnim, *dieAnim ;
    
    ATTACK_INFO* getAttackDamage() ;
    CLASS_TYPE classType ;
    
    std::string weaponSprite ;
    
    int minAttackDamage, maxAttackDamage ;
    int criticalChance ;
    int attackSpeed ;
    
    bool isRetreating ;
};

#endif /* defined(__TapHero__WarriorSprite__) */
