//
//  ComboBarSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#include "ComboBarSprite.h"

bool ComboBar::initWithFile(const std::string &filename)
{
    if ( !Sprite::initWithFile(filename) )
    {
        return false;
    }
    
    return true;
}

void ComboBar::initBar(){
    auto barSize = this->getBoundingBox().size;
    
    comboGuage = 0.0f ;
    
    comboInnerBar = ProgressTimer::create(Sprite::create(COMBO_BAR_INNER)) ;
    
    comboInnerBar->setType(ProgressTimer::Type::BAR);
    comboInnerBar->setMidpoint(Point(0, 0));
    comboInnerBar->setBarChangeRate(Point(0, 1));
    comboInnerBar->setPercentage(comboGuage);
    comboInnerBar->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT) ;
    
    this->addChild(comboInnerBar) ;
}

void ComboBar::increaseGuage(int comboCount){
    
    comboGuage = comboInnerBar->getPercentage() ;
    comboGuage += (1.0+(comboCount*0.05f)) ;
    
    if(comboGuage>100)
        comboGuage = 100 ;
    
    comboInnerBar->setPercentage(comboGuage) ;
    NotificationCenter::getInstance()->postNotification("checkSkillCost", comboInnerBar);
}