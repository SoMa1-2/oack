//
//  StageManager.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 21..
//
//

#ifndef __TapHero__StageManager__
#define __TapHero__StageManager__

#define IS_BONUS "is_bonus"
#define TITLE "title"
#define WAVES "waves"
#define BOSS "boss"

#define KEY_DELAY "delay"
#define KEY_A_KNIGHT "axe_knight"
#define KEY_S_KNIGHT "sword_knight"

#include <stdio.h>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "MonsterSprite.h"
#include "AxeKnightSprite.h"
#include "SwordKnightSprite.h"

USING_NS_CC ;
USING_NS_CC_EXT ;

using namespace rapidjson ;

class StManager {
    
public:
    static StManager* getInstance() ;
    void readFromJson() ;
    
    void init() ;
    
    Vector<Monster*> getCurrentWaveMonsters() ;
    Vector<Monster*> tempMonsters ;
    Vector<Monster*> monsters ;
    
    std::vector<std::vector<std::vector<std::map<const char *, double>>>> table ;
    
    std::string stageTitle ;
    std::string boss ;
    
    float bossScale ;
    
    int waveCount ;
    int currWavePosition ;
};

#endif /* defined(__TapHero__StageManager__) */
