//
//  SlimeSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 15..
//
//

#ifndef __TapHero__SlimeSprite__
#define __TapHero__SlimeSprite__

#define SLIME_NORMAL "slime_normal.png"

#define SLIME_ONWARD "slime_onward.plist"
#define SLIME_ONWARD_FRAME_SIZE 5

#define SLIME_HIT "slime_hit.plist"
#define SLIME_HIT_FRAME_SIZE 1

#define SLIME_DIE "pow.plist"
#define SLIME_DIE_FRAME_SIZE 8

#include <stdio.h>
#include "MonsterSprite.h"

USING_NS_CC ;

class Slime : public Monster{
    
public:
    static Slime* create(){
        auto t = new Slime() ;
        
        if (t->initWithFile(SLIME_NORMAL)) {
            t->initHealthBar() ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
private:
    Slime() ;
    ~Slime() ;
};

#endif /* defined(__TapHero__SlimeSprite__) */
