//
//  CoinSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 16..
//
//

#include "CoinSprite.h"
#include "SpriteFrameManager.h"

Coin::Coin(){
    
    SpFrameManager *frameManager = SpFrameManager::getInstance() ;
    
    shineAnim = frameManager->getAnimFromPlist(COIN_SHINE, "coin", COIN_SHINE_FRAME_SIZE, 0.06f) ;
    spinAnim = frameManager->getAnimFromPlist(COIN_SPIN, "coin_spin", COIN_SPIN_FRAME_SIZE, 0.08f) ;
    
    CC_SAFE_RETAIN(shineAnim) ;
    CC_SAFE_RETAIN(spinAnim) ;
}

void Coin::drop(){
    this->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM) ;
    this->setOpacity(0.0f) ;
    
    ccBezierConfig config ;
    config.controlPoint_1 = Point::ZERO ;
    
    int xAxis = RandomHelper::random_int(5, 20) ;
    
    if(CCRANDOM_0_1()<=0.5f){
        config.controlPoint_2 = Point(5, 50) ;
        config.endPosition = Point(xAxis, 0) ;
    }else{
        config.controlPoint_2 = Point(-5, 50) ;
        config.endPosition = Point(-xAxis, 0) ;
    }
    
    BezierBy *dropAnim = BezierBy::create(0.5f, config);
    
    auto *seq0 = EaseOut::create(dropAnim, 1.5f) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Coin::spin, this));
    
    float randomDelay = CCRANDOM_0_1()-0.35 ;
    if(randomDelay<0)
        randomDelay = 0;
    
    this->runAction(Sequence::createWithTwoActions(DelayTime::create(randomDelay), FadeTo::create(0.0f, 255))) ;
    this->runAction(Sequence::createWithTwoActions(DelayTime::create(randomDelay),
                                                   Sequence::createWithTwoActions(seq0, seq1))) ;
}

void Coin::spin(){
    //    this->stopAllActions() ;
    
    auto *seq0 = Animate::create(spinAnim) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Coin::shine, this)) ;
    
    this->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
}


void Coin::shine(){
    //    this->stopAllActions() ;
    
    auto *seq0 = Animate::create(shineAnim) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Coin::move, this)) ;
    
    this->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
}

void Coin::move(){
    
    Size visibleSize = Director::getInstance()->getVisibleSize() ;
    Vec2 visibleOrigin = Director::getInstance()->getVisibleOrigin() ;
    
    auto *seq0 = EaseOut::create(MoveTo::create(0.5f, Vec2(visibleSize.width+visibleOrigin.x-5, visibleSize.height+visibleOrigin.y-5)), 0.425f) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Coin::done, this)) ;
    
    this->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
}

void Coin::done(){
    NotificationCenter::getInstance()->postNotification("addCoin", this);
    this->removeFromParentAndCleanup(true) ;
}

Coin::~Coin(){
    CC_SAFE_RELEASE_NULL(shineAnim) ;
    CC_SAFE_RELEASE_NULL(spinAnim) ;
}