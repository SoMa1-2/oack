//
//  SwordKnightSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#include "SwordKnightSprite.h"
#include "SpriteFrameManager.h"

SwordKnight::SwordKnight(){
    
    isAddDust = true ;
    
    monsterMaxHp = 126;
    monsterCurrHp = monsterMaxHp;
    
    monsterGold = RandomHelper::random_int(50, 250);
    
    SpFrameManager *frameManager = SpFrameManager::getInstance();
    onwardAnim = RepeatForever::create(Animate::create(frameManager->getAnimFromPlist(S_KNIGHT_ONWARD,
                                                                                      "sword_knight_w",
                                                                                      S_KNIGHT_ONWARD_FRAME_SIZE,
                                                                                      0.125f))) ;
    
    auto *seq0 = Animate::create(frameManager->getAnimFromPlist(S_KNIGHT_HIT, "sword_knight_hit", S_KNIGHT_HIT_FRAME_SIZE, 0.05f)) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Monster::onward, this)) ;
    
    hitAnim = Sequence::createWithTwoActions(seq0, seq1) ;
    dieAnim = Sequence::createWithTwoActions(Animate::create(frameManager->getAnimFromPlist(S_KNIGHT_DIE, "axe_knight_die",
                                                                                            S_KNIGHT_DIE_FRAME_SIZE, 0.05f)), Sequence::createWithTwoActions(DelayTime::create(3.5f), RemoveSelf::create())) ;
    
    
    CC_SAFE_RETAIN(onwardAnim);
    CC_SAFE_RETAIN(hitAnim);
    CC_SAFE_RETAIN(dieAnim);
}

SwordKnight::~SwordKnight(){
    CC_SAFE_RELEASE_NULL(onwardAnim);
    CC_SAFE_RELEASE_NULL(hitAnim);
    CC_SAFE_RELEASE_NULL(dieAnim);
}