//
//  LobbyScene.h
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 10. 5..
//
//

#ifndef __LOBBY_SCENE_H__
#define __LOBBY_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::extension;

class LobbyScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    Size visibleSize;
    Vec2 origin;
    Size winSize;
    
    ui::Scale9Sprite *startBtn;
    Sprite *botBtn[3];
    ui::ScrollView *scroll[3];
    bool isSelected[3];
    
    
    CREATE_FUNC(LobbyScene);
    
    bool onTouchBegan(Touch *touch, Event *unused_event);
};

#endif /* __LOBBY_SCENE_H__ */
