//
//  ScrollViewDelegateLayer.cpp
//  TapHero
//
//  Created by 안범진 on 2015. 9. 30..
//
//

#include "ScrollViewDelegateLayer.h"

ScrollViewDelegateLayer::ScrollViewDelegateLayer()
{
    return;
}
ScrollViewDelegateLayer::~ScrollViewDelegateLayer()
{
    return;
}
bool ScrollViewDelegateLayer::init()
{
    if (!Layer::init())
    {
        return false;
    }
    return true;
}

void ScrollViewDelegateLayer::scrollViewDidScroll(ScrollView* view)
{
    return;
}
void ScrollViewDelegateLayer::scrollViewDidZoom(ScrollView* view)
{
    return;
}