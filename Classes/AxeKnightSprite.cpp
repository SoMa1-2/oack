//
//  AxeKnightSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#include "AxeKnightSprite.h"
#include "SpriteFrameManager.h"

AxeKnight::AxeKnight(){
    
    isAddDust = true ;
    
    monsterMaxHp = 126;
    monsterCurrHp = monsterMaxHp;
    
    monsterGold = RandomHelper::random_int(50, 250);
    
    SpFrameManager *frameManager = SpFrameManager::getInstance();
    onwardAnim = RepeatForever::create(Animate::create(frameManager->getAnimFromPlist(A_KNIGHT_ONWARD,
                                                                                      "axe_knight_w",
                                                                                      A_KNIGHT_ONWARD_FRAME_SIZE,
                                                                                      0.125f))) ;
    
    auto *seq0 = Animate::create(frameManager->getAnimFromPlist(A_KNIGHT_HIT, "axe_knight_hit", A_KNIGHT_HIT_FRAME_SIZE, 0.05f)) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Monster::onward, this)) ;
    
    hitAnim = Sequence::createWithTwoActions(seq0, seq1) ;
    dieAnim = Sequence::createWithTwoActions(Animate::create(frameManager->getAnimFromPlist(A_KNIGHT_DIE, "axe_knight_die",
                                                                                            A_KNIGHT_DIE_FRAME_SIZE, 0.05f)), Sequence::createWithTwoActions(DelayTime::create(3.5f), RemoveSelf::create())) ;
    
    
    CC_SAFE_RETAIN(onwardAnim);
    CC_SAFE_RETAIN(hitAnim);
    CC_SAFE_RETAIN(dieAnim);
}

AxeKnight::~AxeKnight(){
    CC_SAFE_RELEASE_NULL(onwardAnim);
    CC_SAFE_RELEASE_NULL(hitAnim);
    CC_SAFE_RELEASE_NULL(dieAnim);
}