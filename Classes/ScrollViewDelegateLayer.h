//
//  ScrollViewDelegateLayer.h
//  TapHero
//
//  Created by 안범진 on 2015. 9. 30..
//
//

#ifndef __ScrollViewDelegate_Layer_H__
#define __ScrollViewDelegate_Layer_H__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ScrollViewDelegateLayer : public Layer, public ScrollViewDelegate
{
public:
    ScrollViewDelegateLayer(void);
    ~ScrollViewDelegateLayer(void);
    
    // create()의 사용을 위한 함수들
    virtual bool init();
    CREATE_FUNC(ScrollViewDelegateLayer);
    
    // ScrollViewDelegate 상속 메소드 선언
    void scrollViewDidScroll(ScrollView* view);
    void scrollViewDidZoom(ScrollView* view);
};

#endif
