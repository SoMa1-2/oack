//
//  SlimeSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 15..
//
//

#include "SlimeSprite.h"
#include "SpriteFrameManager.h"
#include "CoinSprite.h"

Slime::Slime(){
    
    monsterMaxHp = 126;
    monsterCurrHp = monsterMaxHp;
    
    monsterGold = RandomHelper::random_int(50, 250);
    
    SpFrameManager *frameManager = SpFrameManager::getInstance();
    
    
    
    auto *spa0 = EaseBackOut::create(Animate::create(frameManager->getAnimFromPlist(SLIME_ONWARD, "s", SLIME_ONWARD_FRAME_SIZE, 0.15f))) ;
    auto *spa1 = EaseBackOut::create(MoveBy::create(0.15*5, Vec2(-5, 0))) ;
    
    onwardAnim = Sequence::createWithTwoActions(Spawn::createWithTwoActions(spa0, spa1), CallFunc::create(CC_CALLBACK_0(Monster::onward, this))) ;
    
    auto *seq0 = Animate::create(frameManager->getAnimFromPlist(SLIME_HIT, "h", SLIME_HIT_FRAME_SIZE, 0.05f)) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Monster::onward, this)) ;
    
    hitAnim = Sequence::createWithTwoActions(seq0, seq1) ;
    dieAnim = Sequence::createWithTwoActions(Animate::create(frameManager->getAnimFromPlist(SLIME_DIE, "p", SLIME_DIE_FRAME_SIZE, 0.08f)),
                                             RemoveSelf::create()) ;
    
    
    CC_SAFE_RETAIN(onwardAnim);
    CC_SAFE_RETAIN(hitAnim);
    CC_SAFE_RETAIN(dieAnim);
    
    this->setScale(0.7f) ;
}

Slime::~Slime(){
    CC_SAFE_RELEASE_NULL(onwardAnim);
    CC_SAFE_RELEASE_NULL(hitAnim);
    CC_SAFE_RELEASE_NULL(dieAnim);
}