//
//  TitleScene.h
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 9. 17..
//
//

#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace CocosDenshion;

class TitleScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    Size visibleSize;
    Vec2 origin;
    
    Label *touchLabel;
    Sprite *loginFB;
    
    CREATE_FUNC(TitleScene);
    
    EventListenerTouchOneByOne* listener;
    bool onTouchBegan(Touch *touch, Event *unused_event);
};

#endif /* __TITLE_SCENE_H__ */
