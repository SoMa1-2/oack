#include "HelloWorldScene.h"
#include "TapListLayer.h"

Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);
    
    return scene;
}

bool HelloWorld::init()
{
    if ( !LayerColor::initWithColor(Color4B::BLACK) )
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin() ;
    
    warrior = DevilMage::create() ;
    warrior->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM) ;
    warrior->setPosition(Vec2(visibleSize.width/2+origin.x, visibleSize.height/2+50)) ;
    this->addChild(warrior) ;
    
    //    CC_SAFE_RETAIN(warrior) ;
    
    TapList *tapList = TapList::create() ;
    tapList->mainGameLayer = this ;
    tapList->setPosition(Vec2(0, 0)) ;
    tapList->initSkills(warrior->skills) ;
    this->addChild(tapList) ;
    
    schedule(schedule_selector(HelloWorld::collisionChceck)) ;
    schedule(schedule_selector(HelloWorld::distanceCheck)) ;
//    schedule(schedule_selector(HelloWorld::monsterSchedule), 12.5f) ;
    this->schedule(schedule_selector(HelloWorld::addDust), 0.1f) ;
    
    StManager *manager = StManager::getInstance() ;
    manager->init() ;
    monsters = manager->getCurrentWaveMonsters() ;
    
    waveStart() ;
    
    currentGold = 0 ;
    goldLabel = Label::createWithBMFont("gold_font.fnt", "0");
    goldLabel->setScale(0.4f) ;
    goldLabel->setAnchorPoint(Point::ANCHOR_TOP_RIGHT) ;
    goldLabel->setPosition(Vec2(visibleSize.width+origin.x-5, visibleSize.height+origin.y-5));
    this->addChild(goldLabel) ;
    
    NotificationCenter::getInstance()->addObserver(this, callfuncO_selector(HelloWorld::addCoin), "addCoin", NULL);
    NotificationCenter::getInstance()->addObserver(this, callfuncO_selector(HelloWorld::removeArrow), "removeArrow", NULL);
    
    return true;
}

void HelloWorld::attack(){
    
    warrior->attack() ;
    
    if (warrior->classType == Warrior::CLASS_TYPE::ARCHER ||
        warrior->classType == Warrior::CLASS_TYPE::MAGE) {
        this->runAction(Sequence::createWithTwoActions(DelayTime::create(warrior->attackSpeed/1.5), CallFunc::create(CC_CALLBACK_0(HelloWorld::addSomeAttackObj, this)))) ;
    }
}

void HelloWorld::waveStart(){
    for(Monster *monster : monsters){
        
        monster->setPosition(Vec2(visibleSize.width+origin.x, visibleSize.height/2+50)) ;
        this->addChild(monster) ;
    }
}

void HelloWorld::addSomeAttackObj(){
    Sprite *arrow = warrior->getAttackObj() ;
    
    arrows.pushBack(arrow) ;
    this->addChild(arrow) ;
}

void HelloWorld::monsterSchedule(float dt){
    Monster *monster = SwordKnight::create() ;
    monster->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT) ;
    monster->setPosition(Vec2(visibleSize.width+origin.x, visibleSize.height/2+50)) ;
    monster->onward() ;
    
    monsters.pushBack(monster) ;
    
    this->addChild(monster) ;
}

void HelloWorld::collisionChceck(float dt){
    
    Vector<Sprite*> *willDeleteArrows = new Vector<Sprite*>() ;
    Vector<Monster*> *willDeleteMonsters = new Vector<Monster*>() ;
    
    for (int i=0; i<arrows.size(); i++) {
        
        Sprite *arrow = arrows.at(i);
        for(int j=0; j<monsters.size(); j++){
            
            Monster *monster = monsters.at(j);
            
            Rect arrowRect = arrow->getBoundingBox() ;
            Rect monsterRect = Rect((monster->getPosition().x + monster->getContentSize().width/2),
                                    monster->getPosition().y - (monster->getContentSize().height/2),
                                    monster->getContentSize().width,
                                    monster->getContentSize().height);
            
            
            if (arrowRect.intersectsRect(monsterRect)) {
                willDeleteArrows->pushBack(arrow) ;
                
                Warrior::ATTACK_INFO *info = warrior->getAttackDamage() ;
                monster->attacked(info) ;
                if(monster->getCrrHp()==0)
                    willDeleteMonsters->pushBack(monster) ;
            }
        }
    }
    
    for(auto deleteArrow : *willDeleteArrows){
        arrows.eraseObject(deleteArrow) ;
        this->removeChild(deleteArrow) ;
    }
    
    for(auto deleteMonster : *willDeleteMonsters){
        monsters.eraseObject(deleteMonster) ;
        deleteMonster->die() ;
    }
}

void HelloWorld::removeArrow(Ref *sender){
    
    Sprite *arrow = (Sprite *) sender ;
    arrows.eraseObject(arrow) ;
    
    this->removeChild(arrow) ;
}

void HelloWorld::distanceCheck(float dt){
    
    bool isShouldMoveBack = false ;
    
    for(auto monster : monsters){
        float distance = monster->getPosition().x - warrior->getPosition().x ;
        if(distance < warrior->getContentSize().width)
            isShouldMoveBack = true ;
    }
    
    if(!warrior->isRetreating && isShouldMoveBack){
        warrior->retreat() ;
    }
}

void HelloWorld::addDust(float dt){
    if(warrior->isRetreating){
        
        for(int i=0; i<4; i++){
            
            Sprite *dust = Sprite::create("player_dust.png") ;
            
            int randY = RandomHelper::random_int(-2, 5) ;
            int randX = RandomHelper::random_int(0, 15) ;
            dust->setPosition(warrior->getPosition().x+randX, warrior->getPosition().y+randY) ;
            
            float randScale = (float)RandomHelper::random_int(5, 15) / 10.0f ;
            dust->setScale(randScale) ;
            
            dust->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.25f, 0.0f), RemoveSelf::create())) ;
            this->addChild(dust) ;
        }
    }
}

void HelloWorld::addCoin(Ref *sender){
    Coin *tmpCoin = (Coin *) sender ;
    
    currentGold += tmpCoin->gold ;
    goldLabel->setString(StringUtils::format("%d", currentGold).c_str()) ;
}