//
//  WarriorSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 13..
//
//

#include "WarriorSprite.h"

bool Warrior::initWithFile(const std::string &filename){
    if ( !Sprite::initWithFile(filename) )
    {
        return false;
    }
    return true;
}

Warrior::ATTACK_INFO* Warrior::getAttackDamage(){
    
    ATTACK_INFO *info = (ATTACK_INFO*) malloc(sizeof(ATTACK_INFO)) ;
    info->atkDamage = RandomHelper::random_int(minAttackDamage, maxAttackDamage) ;
    
    if(RandomHelper::random_int(1, 100) <= criticalChance)
        info->isCritical = true ;
    else
        info->isCritical = false ;
    
    return info ;
}

Sprite* Warrior::getAttackObj(){
    
    Sprite *atkObj = Sprite::create(weaponSprite) ;
    atkObj->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT) ;
    
    Vec2 arrowVec = Vec2(this->getPosition().x,
                         this->getPosition().y+this->getBoundingBox().size.height/3.2) ;
    
    atkObj->setPosition(arrowVec) ;
    
    
    auto *seq1 = Sequence::createWithTwoActions(MoveBy::create(2.5, Vec2(300, 0)),
                                                CallFuncN::create(CC_CALLBACK_1(Warrior::attakObjDone, this))) ;
    
    atkObj->runAction(seq1) ;
    atkObj->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.2f, 1.5f), ScaleTo::create(0.1f, 1.0f)))) ;
    
    return atkObj ;
}

void Warrior::attakObjDone(cocos2d::Ref *sender){
    NotificationCenter::getInstance()->postNotification("removeArrow", sender);
}

void Warrior::attack(){
    
    isRetreating = false ;
    
    this->stopAllActions() ;
    this->runAction(Sequence::createWithTwoActions(Animate::create(attackAnim), CallFunc::create(CC_CALLBACK_0(Warrior::idle, this))));
}

void Warrior::idle(){
    
    isRetreating = false ;
    
    this->stopAllActions() ;
    this->runAction(RepeatForever::create(Animate::create(idleAnim)));
}

void Warrior::retreat(){
    
    isRetreating = true ;
    
    this->stopAllActions() ;
    
    auto *seq0 = Spawn::createWithTwoActions(EaseExponentialInOut::create(MoveBy::create(0.4f, Vec2(-(this->getContentSize().width/1.5f), 0))), Animate::create(retreatAnim)) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Warrior::idle, this)) ;
    
    this->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
}