//
//  TapSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 11..
//
//

#ifndef __TapHero__TapSprite__
#define __TapHero__TapSprite__

#include <stdio.h>
#include "cocos2d.h"

class Tap : public cocos2d::Sprite{

public:
    virtual bool initWithFile(const std::string &filename);
    CREATE_FUNC(Tap);
    
    bool getIsAdWindow() ;
    void setIsAdWindow(bool isThisAdWindow) ;
    
    static Tap* create(const std::string &filename){
        auto *t = new Tap() ;
        
        if (t->initWithFile(filename)) {
            t->autorelease() ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
private:
    bool isAdWindow ;
} ;

#endif /* defined(__TapHero__TapSprite__) */
