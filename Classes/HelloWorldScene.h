#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ArcherSprite.h"
#include "DevilMageSprite.h"
#include "SlimeSprite.h"
#include "CoinSprite.h"
#include "AxeKnightSprite.h"
#include "SwordKnightSprite.h"

#include "StageManager.h"

USING_NS_CC ;

class HelloWorld : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    void attack() ;
    void addSomeAttackObj() ;
    void waveStart() ;
    
    DevilMage *warrior ;
    Vector<Sprite*> arrows ;
    Vector<Monster*> monsters ;
    
    Size visibleSize ;
    Vec2 origin ;
    
    int currentGold ;
    Label *goldLabel ;
    
    CREATE_FUNC(HelloWorld);
    
    void monsterSchedule(float dt) ;
    void collisionChceck(float dt) ;
    void distanceCheck(float dt) ;
    void addDust(float dt) ;
    
    void removeArrow(Ref *sender) ;
    void addCoin(Ref *sender) ;
};

#endif // __HELLOWORLD_SCENE_H__
