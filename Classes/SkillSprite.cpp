//
//  SkillIconSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 21..
//
//

#include "SkillSprite.h"

Skill::Skill(){
    
    isOverCost = false ;
    isCooldown = false ;
    
    initCoolTimeProgress() ;
    NotificationCenter::getInstance()->addObserver(this, callfuncO_selector(Skill::checkSkillCost), "checkSkillCost", NULL);
}

void Skill::initCoolTimeProgress(){
    
    coolTimeProgress = ProgressTimer::create(Sprite::create("skill_cool_time.png")) ;
    
    coolTimeProgress->setType(ProgressTimer::Type::RADIAL);
    coolTimeProgress->setReverseProgress(true) ;
    coolTimeProgress->setPercentage(100.0f);
    coolTimeProgress->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT) ;
    coolTimeProgress->setPosition(Vec2::ZERO);
    
    this->addChild(coolTimeProgress) ;
}

void Skill::checkSkillCost(cocos2d::Ref *sender){
    
    if(isCooldown){
        return ;
    }
    
    comboGuage = (ProgressTimer *) sender ;
    
    if(comboGuage->getPercentage() >= skillData.costGuage){
        if(!isOverCost){
            
            coolTimeProgress->setPercentage(0.0f) ;
            showShineEffect() ;
            isOverCost = true ;
        }
    }else{
        if(!isCooldown){
            coolTimeProgress->setPercentage(100.0f);
            isOverCost = false ;
        }
    }
}

void Skill::showShineEffect(){
    Sprite *atkShine = Sprite::create("atk_shine.png") ;
    atkShine->cocos2d::Node::setPosition(Vec2(this->getBoundingBox().size.width/2,
                                              this->getBoundingBox().size.height/2)) ;
    
    auto *scaleAnim = Sequence::createWithTwoActions(EaseBackOut::create(ScaleTo::create(0.65f, 4.5f)),
                                                     ScaleTo::create(0.65f, 0.0f)) ;
    auto *rotateAnim = RotateBy::create(1.3f, 360) ;
    
    auto *seq0 = Spawn::createWithTwoActions(scaleAnim, rotateAnim) ;
    auto *seq1 = RemoveSelf::create() ;
    
    atkShine->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
    
    this->addChild(atkShine) ;
}

void Skill::useSkill(){
    if(!isCooldown && isOverCost){
        
        comboGuage->setPercentage(comboGuage->getPercentage() - skillData.costGuage) ;
        
        isCooldown = true ;
        
        auto seq0 = ProgressFromTo::create(skillData.skillCooltime, 100, 0) ;
        auto seq1 = CallFunc::create(CC_CALLBACK_0(Skill::cooldownDone, this)) ;
        
        coolTimeProgress->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
        
        NotificationCenter::getInstance()->postNotification("checkSkillCost", comboGuage);
    }
}

void Skill::cooldownDone(){
    
    if(comboGuage->getPercentage() >= skillData.costGuage){
        coolTimeProgress->setPercentage(0.0f) ;
        showShineEffect() ;
        isOverCost = true ;
    }else{
        coolTimeProgress->setPercentage(100.0f) ;
        isOverCost = false ;
    }
    
    isCooldown = false ;
}

Skill::~Skill(){
    //    CC_SAFE_FREE(skillData) ;
}