//
//  AxeKnightSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#ifndef __TapHero__AxeKnightSprite__
#define __TapHero__AxeKnightSprite__

#define A_KNIGHT_NORMAL "axe_knight_normal.png"

#define A_KNIGHT_ONWARD "axe_knight_walk.plist"
#define A_KNIGHT_ONWARD_FRAME_SIZE 4

#define A_KNIGHT_HIT "axe_knight_hit.plist"
#define A_KNIGHT_HIT_FRAME_SIZE 1

#define A_KNIGHT_DIE "axe_knight_die.plist"
#define A_KNIGHT_DIE_FRAME_SIZE 5


#include <stdio.h>
#include "cocos2d.h"
#include "MonsterSprite.h"

USING_NS_CC ;

class AxeKnight : public Monster{
    
public:
    static AxeKnight* create(){
        auto t = new AxeKnight() ;
        
        if (t->initWithFile(A_KNIGHT_NORMAL)) {
            t->initHealthBar() ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }
    
private:
    AxeKnight() ;
    ~AxeKnight() ;
};

#endif /* defined(__TapHero__AxeKnightSprite__) */
