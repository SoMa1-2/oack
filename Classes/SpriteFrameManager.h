//
//  SpriteFrameManager.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 16..
//
//

#ifndef __TapHero__SpriteFrameManager__
#define __TapHero__SpriteFrameManager__

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC ;

class SpFrameManager {
public:
    static SpFrameManager* getInstance() ;
    Animation* getAnimFromPlist(const std::string &plist, std::string id, int frameSize, float interval) ;
    
private:
    SpriteFrameCache *spritecache ;
    
    SpFrameManager() ;
    ~SpFrameManager() ;
};

#endif /* defined(__TapHero__SpriteFrameManager__) */
