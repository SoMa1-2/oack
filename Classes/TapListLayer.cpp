#include "TapListLayer.h"

bool TapList::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    this->ignoreAnchorPointForPosition(false) ;
    this->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT) ;
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    initComboBar() ;
    initTapList() ;
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    listener->onTouchCancelled = CC_CALLBACK_2(HelloWorld::onTouchCancelled, this);
    
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    isTouchDown = false;
    
    initialTouchPos[0] = 0;
    initialTouchPos[1] = 0;
    
    index = -1 ;
    
    return true;
}

void TapList::initComboBar(){
    comboGauge = ComboBar::create() ;
    comboGauge->setPosition(Vec2(visibleSize.width + origin.x - (comboGauge->getBoundingBox().size.width*1.5),
                                 visibleSize.height/3)) ;
    this->addChild(comboGauge, -1) ;
}

void TapList::initSkills(Vector<Skill*> skills){
    
    this->skills = skills ;
    
    float tapWidth = lastWindow->getBoundingBox().size.width ;
    float winSize = Director::getInstance()->getWinSize().width ;
    
    int i=0 ;
    for(Skill *tmpSkill : skills){
        
        if(i==0){
            tmpSkill->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT) ;
            tmpSkill->setPosition(Vec2((winSize-tapWidth)/2, 3.0f)) ;
        }else if(i==1){
            tmpSkill->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM) ;
            tmpSkill->setPosition(Vec2(winSize/2, 3.0f)) ;
        }else{
            tmpSkill->setAnchorPoint(Point::ANCHOR_BOTTOM_RIGHT) ;
            tmpSkill->setPosition(Vec2((winSize-tapWidth)/2+tapWidth, 3.0f)) ;
        }

        
        i++ ;
        this->addChild(tmpSkill) ;
    }
}

void TapList::initTapList(){
    
    canTapList = new Vector<Tap*>() ;
    
    for(int i=0; i<MAX_WINDOW_COUNT; i++){
        
        Tap *window = this->getNewWindow() ;
        window->setPosition(Vec2(visibleSize.width/2 + origin.x,
                                 visibleSize.height/3+30 - (WINDOW_OFFSET*i))) ;
        
        window->setScale(0.65+(0.05*i)) ;
        this->addChild(window) ;
        canTapList->pushBack(window) ;
        
        if(i==0)
            firstWindowPos = window->getPosition() ;
        
        if(i==(MAX_WINDOW_COUNT-1)){
            lastWindow = window ;
            lastWindowPos = this->lastWindow->getPosition() ;
        }
    }
}

Point TapList::touchToPoint(Touch* touch)
{
    return CCDirector::getInstance()->convertToGL(touch->getLocationInView());
}

bool TapList::isTouchingSprite(Touch* touch){
    return this->lastWindow->getBoundingBox().containsPoint(this->touchToPoint(touch));
}

bool TapList::onTouchBegan(Touch* touch, Event* unused_event){
    
    initialTouchPos[0] = touch->getLocation().x;
    initialTouchPos[1] = touch->getLocation().y;
    currentTouchPos[0] = touch->getLocation().x;
    currentTouchPos[1] = touch->getLocation().y;
    
    isTouchDown = true;
    
    touchBeganPos = this->touchToPoint(touch) ;
    this->touchOffset = this->lastWindow->getPosition() - this->touchToPoint(touch);
    
    return true;
    
}

void TapList::onTouchMoved(Touch* touch, Event* unused_event){
    
    if(isTouchingSprite(touch)){
        currentTouchPos[0] = touch->getLocation().x;
        currentTouchPos[1] = touch->getLocation().y;
        this->lastWindow->setPosition(this->touchToPoint(touch) + this->touchOffset);
    }
}

void TapList::onTouchCancelled(Touch* touch, Event* unused_event){}

void TapList::onTouchEnded(Touch* touch, Event* unused_event){
    
    this->touchEndPos = this->touchToPoint(touch) ;
    
    for(Skill *tmpSkill : skills){
        if(tmpSkill->getBoundingBox().containsPoint(touchEndPos)){
            tmpSkill->useSkill() ;
            return ;
        }
    }
    
    if(!isTouchingSprite(touch))
        return ;
    
    Director::getInstance()->getEventDispatcher()->pauseEventListenersForTarget(this);
    this->runAction(Sequence::createWithTwoActions(DelayTime::create(0.075f),
                                                   CallFunc::create(CC_CALLBACK_0(TapList::resumeTouch, this)))) ;
    
    // set the new sprite position
    this->lastWindow->setPosition(this->touchToPoint(touch) + this->touchOffset);
    this->lastWindow->runAction(MoveTo::create(0.2f, lastWindowPos)) ;
    
    float distance = touchBeganPos.getDistanceSq(touchEndPos) ;
    
    if(lastWindow->getIsAdWindow()){
        if(distance < 50){
            comboCount = 0 ;
        }else{
            bool isSwiped = false ;
            
            if (initialTouchPos[0] - currentTouchPos[0] > visibleSize.width * 0.05){
                CCLOG("SWIPED LEFT");
                lastWindow->runAction(Sequence::createWithTwoActions(MoveTo::create(0.5f, Vec2(-200, 450)), RemoveSelf::create())) ;
                isSwiped = true ;
            }else if (initialTouchPos[0] - currentTouchPos[0] < -visibleSize.width * 0.05){
                CCLOG("SWIPED RIGHT");
                lastWindow->runAction(Sequence::createWithTwoActions(MoveTo::create(0.5f, Vec2(visibleSize.width+400, 450)), RemoveSelf::create())) ;
                isSwiped = true ;
            }else if (initialTouchPos[1] - currentTouchPos[1] > visibleSize.width * 0.05){
                CCLOG("SWIPED DOWN");
                lastWindow->runAction(Sequence::createWithTwoActions(MoveTo::create(0.5f, Vec2(visibleSize.width/2, -450)), RemoveSelf::create())) ;
                isSwiped = true ;
            }else if (initialTouchPos[1] - currentTouchPos[1] < -visibleSize.width * 0.05){
                CCLOG("SWIPED UP");
                lastWindow->runAction(Sequence::createWithTwoActions(MoveTo::create(0.5f, Vec2(visibleSize.width/2, visibleSize.height+450)), RemoveSelf::create())) ;
                isSwiped = true ;
            }
            
            if(isSwiped){
                
                canTapList->popBack() ;
                
                lastWindow = canTapList->at(canTapList->size()-1) ;
                addWindow() ;
            }
        }
    }else{
        if(distance < 50){
            comboCount++ ;
            
            this->removeChild(lastWindow) ;
            canTapList->popBack() ;
            
            lastWindow = canTapList->at(canTapList->size()-1) ;
            addWindow() ;
            
            this->mainGameLayer->attack() ;
        }else{
            comboCount = 0 ;
        }
        
        this->showComboCount(this->touchToPoint(touch)) ;
    }
    
    isTouchDown = false;
}

void TapList::showComboCount(Point touchEndPoint){
    if(comboCount>5){
        
        auto *comboLabel = Label::createWithBMFont("combo_font.fnt", StringUtils::format("%d Combo!", comboCount).c_str());
        comboLabel->setPosition(Vec2(touchEndPoint.x, touchEndPoint.y+10)) ;
        comboLabel->setScale(0.5f) ;
        
        ccBezierConfig config ;
        config.controlPoint_1 = Point::ZERO ;
        
        int xAxis = RandomHelper::random_int(5, 20) ;
        
        if(CCRANDOM_0_1()<=0.5f){
            config.controlPoint_2 = Point(20, 60) ;
            config.endPosition = Point(xAxis, 0) ;
        }else{
            config.controlPoint_2 = Point(-20, 60) ;
            config.endPosition = Point(-xAxis, 0) ;
        }
        
        auto *dropAnim = Spawn::createWithTwoActions(BezierBy::create(0.5f, config), FadeTo::create(0.5f, 0));
        comboLabel->runAction(Sequence::createWithTwoActions(dropAnim, RemoveSelf::create())) ;
        
        this->addChild(comboLabel) ;
    }
    
    if(comboCount>0)
        comboGauge->increaseGuage(comboCount) ;
}

Tap* TapList::getNewWindow(){
    Tap *window = NULL ;
    
    int randInt = RandomHelper::random_int(0, MAX_WINDOW_COUNT) ;
    //    int randInt = 1 ;
    if(randInt==0){
        window = Tap::create(AD_WINDOW) ;
        window->setIsAdWindow(true) ;
    }else{
        window = Tap::create(NORMAL_WINDOW) ;
        window->setIsAdWindow(false) ;
    }
    
    return window ;
}

void TapList::addWindow(){
    
    Tap *newWindow = this->getNewWindow() ;
    
    canTapList->insert(0, newWindow) ;
    this->addChild(newWindow, index--) ;
    
    for(int i=0; i<canTapList->size(); i++) {
        
        Tap *window = canTapList->at(i) ;
        if(i==0){
            window->setPosition(firstWindowPos) ;
            window->setScale(0.6f) ;
            
            auto scaleTo = ScaleTo::create(0.1f, 0.65f) ;
            auto moveTo = MoveTo::create(0.1f, Vec2(visibleSize.width/2 + origin.x,
                                                    visibleSize.height/3+30)) ;
            
            window->runAction(Spawn::createWithTwoActions(scaleTo, moveTo)) ;
            
        }else{
            auto scaleTo = ScaleTo::create(0.1f, window->getScale()+0.05f) ;
            auto moveTo = MoveTo::create(0.1f, Vec2(window->getPosition().x,
                                                    window->getPosition().y-WINDOW_OFFSET)) ;
            
            window->runAction(Spawn::createWithTwoActions(scaleTo, moveTo)) ;
        }
    }
}

void TapList::resumeTouch(){
    Director::getInstance()->getEventDispatcher()->resumeEventListenersForTarget(this);
}

void TapList::removeWindow(){
    canTapList->erase(0) ;
}
