//
//  ComboBarSprite.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 20..
//
//

#ifndef __TapHero__ComboBarSprite__
#define __TapHero__ComboBarSprite__

#include <stdio.h>
#include "cocos2d.h"

#define COMBO_BAR_OUTER "combo_outer.png"
#define COMBO_BAR_INNER "combo_inner.png"

USING_NS_CC ;

class ComboBar : public Sprite{
    
public:
    virtual bool initWithFile(const std::string &filename);
    
    static ComboBar* create(){
        auto t = new ComboBar() ;
        
        if (t->initWithFile(COMBO_BAR_OUTER)) {
            t->initBar() ;
            return t;
        } else {
            delete t;
            return nullptr;
        }
    }

    void initBar() ;
    void increaseGuage(int comboCount) ;
    
private:
    ProgressTimer *comboInnerBar;
    float comboGuage ;
    
};

#endif /* defined(__TapHero__ComboBarSprite__) */
