//
//  TapSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 11..
//
//

#include "TapSprite.h"

bool Tap::initWithFile(const std::string &filename)
{
    if ( !Sprite::initWithFile(filename) )
    {
        return false;
    }
    
    return true;
}

bool Tap::getIsAdWindow(){
    return isAdWindow ;
}

void Tap::setIsAdWindow(bool isThisAdWindow){
    this->isAdWindow = isThisAdWindow ;
}
