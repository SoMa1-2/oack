//
//  SpriteFrameManager.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 16..
//
//

#include "SpriteFrameManager.h"

static SpFrameManager *manager = nullptr ;
SpFrameManager* SpFrameManager::getInstance(){
    if(!manager)
        manager = new SpFrameManager() ;
    
    return manager ;
}

SpFrameManager::SpFrameManager(){
    spritecache = SpriteFrameCache::getInstance();
}

Animation* SpFrameManager::getAnimFromPlist(const std::string &plist, std::string id, int frameSize, float interval){
    
    spritecache->addSpriteFramesWithFile(plist);
    
    Vector<SpriteFrame*> animFrames;
    char str[100];
    for(int i = 1; i <= frameSize; i++)
    {
        sprintf(str, "%s_%d.png", id.c_str(), i);
        animFrames.pushBack(spritecache->getSpriteFrameByName(str));
    }
    
    Animation *animation = Animation::createWithSpriteFrames(animFrames, interval);
    return animation ;
}

SpFrameManager::~SpFrameManager(){
    
}
