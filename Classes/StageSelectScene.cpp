//
//  StageSelectScene.cpp
//  TapHero
//
//  Created by Beomjin Ahn on 2015. 9. 29..
//
//

#include "StageSelectScene.h"
#include "HelloWorldScene.h"

Scene* StageSelectScene::createScene()
{
    auto scene = Scene::create();
    auto layer = StageSelectScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool StageSelectScene::init()
{
    //initWithColor를 사용하여 scene 배경색 설정
    if (!LayerColor::initWithColor(*new Color4B(0,31,66,255)))
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    winSize = Director::getInstance()->getWinSize();
    
    //뒤로가기 버튼
    auto backBtn = Button::create("backbtn.png");
    backBtn->setScale(0.3);
    backBtn->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    backBtn->setPosition(Vec2(origin.x+3.0,origin.y+3.0));
    backBtn->addTouchEventListener( [](Ref* pSender, Widget::TouchEventType type)
    {
        if (type == Widget::TouchEventType::ENDED)
        {
            Director::getInstance()->popScene();
        }
    });
    this->addChild(backBtn);
    
    //배경 별
    for(int i=0;i<10;i++)
    {
        auto star = Label::createWithTTF("*", "fonts/Marker Felt.ttf", 14);
        
        int rand = RandomHelper::random_int(1, 1000000);
        star->setPosition(Vec2(rand%(int)visibleSize.width+origin.x, visibleSize.height*i/10+origin.y));
        star->setAlignment(TextHAlignment::CENTER);
        star->setColor(Color3B::YELLOW);
        
        this->addChild(star);
    }
    
    //스크롤뷰
    auto container = Layer::create();
    
    auto scrollView = ui::ScrollView::create();
    scrollView->setBackGroundColorType(LAYOUT_COLOR_SOLID);
    scrollView->setBackGroundColorOpacity(0);
    scrollView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    scrollView->setPosition(Vec2(winSize.width/2, winSize.height/2));
    scrollView->setDirection(SCROLLVIEW_DIR_VERTICAL);
    scrollView->setSwallowTouches(false);

    float towerbodyheight;
    int STAGE_NUM=100;
    
    for(int i=0;i<STAGE_NUM;i++)
    {
        auto tower = Sprite::create("tower_body.png");
        towerbodyheight=tower->getContentSize().height;
        tower->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        tower->setPosition(Vec2(visibleSize.width/2, (STAGE_NUM-1-i)*towerbodyheight));
        container->addChild(tower);
        
        char str[5];
        sprintf(str,"%d",i+1);
        
        auto button = Button::create();
        button->setPosition(Vec2(visibleSize.width/2, (STAGE_NUM-1-i)*towerbodyheight+15));
        button->setTitleText(str);
        button->setTitleFontName("fonts/RetroComputer.ttf");
        button->setTitleFontSize(14);
        button->addTouchEventListener( [](Ref* pSender, Widget::TouchEventType type)
                                       {
                                           if (type == Widget::TouchEventType::ENDED)
                                           {
                                               // (i+1)stage
                                               Director::getInstance()->pushScene(HelloWorld::createScene());
                                           }
                                       });
        container->addChild(button);
    }
    
    auto towerhead = Sprite::create("tower_head.png");
    towerhead->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    towerhead->setPosition(Vec2(visibleSize.width/2, STAGE_NUM*towerbodyheight));
    container->addChild(towerhead);
    
    scrollView->addChild(container);
    scrollView->setSize(Size(visibleSize.width, visibleSize.height));
    scrollView->setInnerContainerSize(Size(visibleSize.width, STAGE_NUM*towerbodyheight+towerhead->getContentSize().height));
    this->addChild(scrollView);
    
    return true;
}