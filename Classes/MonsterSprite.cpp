//
//  MonsterSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 13..
//
//

#include "MonsterSprite.h"
#include "CoinSprite.h"

bool Monster::initWithFile(const std::string &filename){
    if ( !Sprite::initWithFile(filename) )
    {
        return false;
    }
    return true;
}

float Monster::getCrrHp(){
    return monsterCurrHp ;
}

void Monster::initHealthBar(){
    auto monsterSize = this->getBoundingBox().size;
    
    monsterMoveSpeed = 0.1f ;
    
    monsterHpBar = ProgressTimer::create(Sprite::create("hp_bar.png")) ;
    
    monsterHpBar->setType(ProgressTimer::Type::BAR);
    monsterHpBar->setMidpoint(Point(0, 0));
    monsterHpBar->setBarChangeRate(Point(1, 0));
    monsterHpBar->setPercentage(100.0f);
    monsterHpBar->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM) ;
    monsterHpBar->setPosition(Point(monsterSize.width/2, monsterSize.height/2+10));
    
    this->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT) ;
    this->addChild(monsterHpBar) ;
}

void Monster::onward(){
    this->stopAllActions() ;
    this->runAction(onwardAnim) ;
}

void Monster::attacked(Warrior::ATTACK_INFO *attackInfo){
    this->stopAllActions() ;
    this->runAction(hitAnim) ;
    
    bool isCritical = attackInfo->isCritical ;
    int damage = attackInfo->atkDamage ;
    
    if(isCritical)
        damage*=2.5 ;
    
    monsterCurrHp -= damage ;
    if(monsterCurrHp < 0)
        monsterCurrHp = 0 ;
    
    monsterHpBar->setPercentage((float)monsterCurrHp/monsterMaxHp*100) ;
    
    Label *damageLabel = Label::createWithSystemFont(Value(damage).asString(), "", 10) ;
    
    if(isCritical){
        damageLabel->setTextColor(Color4B::RED) ;
        damageLabel->setScale(2.0f) ;
    }
    
    damageLabel->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM) ;
    damageLabel->setPosition(Point(this->getBoundingBox().size.width/2, this->getBoundingBox().size.height+15)) ;
    
    auto *upAnim = EaseExponentialOut::create(MoveBy::create(0.5f, Vec2(0, 15))) ;
    auto *fadeOutAnim = EaseExponentialOut::create(FadeOut::create(0.5f)) ;
    
    damageLabel->runAction(Sequence::createWithTwoActions(Spawn::createWithTwoActions(upAnim, fadeOutAnim),
                                                          RemoveSelf::create())) ;
    this->addChild(damageLabel) ;
    this->setPosition(Point(this->getPosition().x+2.5, this->getPosition().y)) ;
    
    CC_SAFE_FREE(attackInfo) ;
}

void Monster::healing(int heal){
    monsterCurrHp += heal ;
    if(monsterCurrHp > monsterMaxHp)
        monsterCurrHp = monsterMaxHp ;
}


void Monster::die(){
    isDie = true ;
    int coinCount = monsterGold/GOLD_UNIT ;
    
    for(int i=0; i<coinCount; i++){
        Coin *coin ;
        
        if(i==(coinCount-1))
            coin = Coin::create(GOLD_UNIT+(monsterGold%GOLD_UNIT)) ;
        else
            coin = Coin::create(GOLD_UNIT) ;
        
        coin->setPosition(this->getPosition()) ;
        coin->drop() ;
        
        this->getParent()->addChild(coin) ;
    }
    
    this->stopAllActions() ;
    
    auto *delay = DelayTime::create(0.15) ;
    auto *call = CallFunc::create(CC_CALLBACK_0(Monster::addDust, this)) ;
    
    this->runAction(dieAnim) ;
    if(isAddDust)
        this->runAction(Sequence::createWithTwoActions(delay, call)) ;
}

void Monster::addDust(){
    for(int i=0; i<8; i++){
        
        Sprite *dust = Sprite::create("player_dust.png") ;
        
        float randY = RandomHelper::random_int(-5, 5)/5.0f ;
        float randX = RandomHelper::random_int(0, 35)/5.0f ;
        dust->setPosition(randX, randY) ;
        
        float randScale = (float)RandomHelper::random_int(5, 15) / 10.0f ;
        dust->setScale(randScale) ;
        
        dust->runAction(Sequence::createWithTwoActions(ScaleTo::create(0.25f, 0.0f), RemoveSelf::create())) ;
        this->addChild(dust) ;
    }
}

void Monster::setDelay(float delay){
    
    float randomDelay = CCRANDOM_0_1()*2.0f ;
    randomDelay+=delay ;
    
    
    auto *seq0 = DelayTime::create(randomDelay) ;
    auto *seq1 = CallFunc::create(CC_CALLBACK_0(Monster::onSchedule, this)) ;
    
    this->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
}

void Monster::onSchedule(){
    isDie = false ;
    onward() ;
    schedule(schedule_selector(Monster::move), 0.125f) ;
}

void Monster::move(float dt){
    if(!isDie)
        this->setPosition(this->getPosition().x-1, this->getPosition().y) ;
}