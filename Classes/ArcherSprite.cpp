//
//  ArcherSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 15..
//
//

#include "ArcherSprite.h"

Archer::Archer(){
    
    classType = CLASS_TYPE::ARCHER ;
    
    minAttackDamage = 7 ;
    maxAttackDamage = 12 ;
    
    criticalChance = 10 ;
    attackSpeed = 0.12f ;
    weaponSprite = "arrow.png" ;
    
    SpFrameManager *frameManager = SpFrameManager::getInstance() ;
    
    idleAnim = frameManager->getAnimFromPlist(ARCHER_IDLE, "i", ARCHER_IDLE_FRAME_SIZE, 1.0f/6) ;
    attackAnim = frameManager->getAnimFromPlist(ARCHER_ATK, "a", ARCHER_ATK_FRAME_SIZE, 0.03f) ;
    retreatAnim = frameManager->getAnimFromPlist(ARCHER_RETREAT, "r", ARCHER_RETREAT_FRAME_SIZE, 0.08f) ;
    
    CC_SAFE_RETAIN(idleAnim) ;
    CC_SAFE_RETAIN(attackAnim) ;
    CC_SAFE_RETAIN(retreatAnim) ;
    
    this->idle() ;
}

Archer::~Archer(){
    CC_SAFE_RELEASE_NULL(idleAnim) ;
    CC_SAFE_RELEASE_NULL(attackAnim) ;
    CC_SAFE_RELEASE_NULL(retreatAnim) ;
}
