//
//  DevilMageSprite.cpp
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 19..
//
//

#include "DevilMageSprite.h"

DevilMage::DevilMage(){
    
    classType = CLASS_TYPE::MAGE ;
    
    SKILL_INFO summon_monster ;
    
    summon_monster.skillCooltime = 7.5f ;
    summon_monster.costGuage = 35 ;
    summon_monster.skillIconPath = "skill_summon.png" ;
    summon_monster.skillType = SKILL_TYPE::SUMMON_MONSTER ;
    
    SKILL_INFO cleave ;
    cleave.skillCooltime = 15.5 ;
    cleave.costGuage = 65 ;
    cleave.skillIconPath = "skill_cleave.png" ;
    cleave.skillType = SKILL_TYPE::METEOR ;
    
    SKILL_INFO meteor ;
    meteor.skillCooltime = 24 ;
    meteor.costGuage = 100 ;
    meteor.skillIconPath = "skill_meteor.png" ;
    meteor.skillType = SKILL_TYPE::METEOR ;
    
    skills.pushBack(Skill::create(summon_monster)) ;
    skills.pushBack(Skill::create(cleave)) ;
    skills.pushBack(Skill::create(meteor)) ;
    
    minAttackDamage = 7 ;
    maxAttackDamage = 12 ;
    
    criticalChance = 10 ;
    attackSpeed = 0.12f ;
    weaponSprite = "blue_ball.png" ;
    
    SpFrameManager *frameManager = SpFrameManager::getInstance() ;
    
    idleAnim = frameManager->getAnimFromPlist(DEVIL_IDLE, "devil", DEVIL_IDLE_FRAME_SIZE, 0.45f) ;
    attackAnim = frameManager->getAnimFromPlist(DEVIL_ATK, "devil_atk", DEVIL_ATK_FRAME_SIZE, 0.15f) ;
    retreatAnim = frameManager->getAnimFromPlist(DEVIL_BACK, "devil_back", DEVIL_BACK_FRAME_SIZE, 0.1f) ;
    
    CC_SAFE_RETAIN(idleAnim) ;
    CC_SAFE_RETAIN(attackAnim) ;
    CC_SAFE_RETAIN(retreatAnim) ;
    
    this->idle() ;
}

void DevilMage::attack(){
    
    Sprite *atkShine = Sprite::create("atk_shine.png") ;
    atkShine->cocos2d::Node::setPosition(Vec2(5.5, this->getBoundingBox().size.height)) ;
    
    auto *scaleAnim = EaseBackOut::create(ScaleTo::create(0.15f, 2.5f)) ;
    auto *rotateAnim = RotateBy::create(0.15f, 360) ;
    
    auto *seq0 = Spawn::createWithTwoActions(scaleAnim, rotateAnim) ;
    auto *seq1 = RemoveSelf::create() ;
    
    atkShine->runAction(Sequence::createWithTwoActions(seq0, seq1)) ;
    this->addChild(atkShine) ;
    
    Warrior::attack() ;
}

DevilMage::~DevilMage(){
    CC_SAFE_RELEASE_NULL(idleAnim) ;
    CC_SAFE_RELEASE_NULL(attackAnim) ;
    CC_SAFE_RELEASE_NULL(retreatAnim) ;
}
