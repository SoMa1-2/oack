//
//  TapListLayer.h
//  TapHero
//
//  Created by KimCheolyong on 2015. 9. 11..
//
//

#ifndef __TapHero__TapListLayer__
#define __TapHero__TapListLayer__

#define NORMAL_WINDOW "normal_window.png"
#define AD_WINDOW "ad_window.png"

#define MAX_WINDOW_COUNT 7
#define WINDOW_OFFSET 6.5f

#include <stdio.h>
#include "cocos2d.h"
#include "TapSprite.h"
#include "HelloWorldScene.h"
#include "ComboBarSprite.h"
#include "SkillSprite.h"

USING_NS_CC ;

class TapList : public cocos2d::Layer{
    
public:
    virtual bool init();
    CREATE_FUNC(TapList);
    
    HelloWorld *mainGameLayer ;
    
    Tap* lastWindow;
    LayerColor* colorLayer;
    Point touchOffset;
    
    Point firstWindowPos, lastWindowPos ;
    Point touchBeganPos, touchEndPos ;
    
    Size visibleSize ;
    Vec2 origin ;
    
    ComboBar *comboGauge ;
    
    Tap* getNewWindow() ;
    void addWindow() ;
    void removeWindow() ;
    void showComboCount(Point touchEndPoint) ;
    
    void initSkills(Vector<Skill*> skills) ;
    
    int index ;
    int comboCount ;
    
    bool isTouchDown;
    
    float initialTouchPos[2];
    float currentTouchPos[2];
    
    virtual bool onTouchBegan(Touch* touch, Event* unused_event);
    virtual void onTouchMoved(Touch* touch, Event* unused_event);
    virtual void onTouchEnded(Touch* touch, Event *unused_event);
    virtual void onTouchCancelled(Touch* touch, Event* unused_event);
    
private:
    void initComboBar() ;
    void initTapList() ;
    
    cocos2d::Vector<Tap*> *canTapList ;
    
    Point touchToPoint(Touch* touch) ;
    Vector<Skill*> skills ;
    
    bool isTouchingSprite(Touch* touch) ;
    
    void resumeTouch() ;
};

#endif /* defined(__TapHero__TapListLayer__) */
