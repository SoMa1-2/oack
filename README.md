# O.A.C.K

<슈퍼캐주얼 게임 만들기>  
SoMa 6th 오우택 멘토님 프로젝트입니다.

진행멤버  
  - 오우택 멘토님  
  - 최관호  
  - 김철용  
  - 안범진  

프로젝트 개발환경 설정은 [Cocos2d-X 3.8][cocos2dx] / [Android ndk-r10][ndk] 기준으로 설정하였습니다. 

> 프로젝트 진행은 맥 환경에서의 경우 Xcode/Eclipse로 진행하며, 윈도우의 경우 Visual Studio 2013/Eclipse로 진행합니다.

### Version
BETA 1.0.0

   [cocos2dx]: <http://www.cocos2d-x.org/download>
   [ndk]: <https://developer.android.com/ndk/downloads/index.html>